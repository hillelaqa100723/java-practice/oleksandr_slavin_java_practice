package hw4;

public class ArrayPalindromeChecker {
    public static void checkArrayPalindrome(int[] arr) {
        boolean isPalindrome = true;
        int left = 0;
        int right = arr.length - 1;

        while (left < right) {
            if (arr[left] != arr[right]) {
                isPalindrome = false;
                break;
            }
            left++;
            right--;
        }

        if (isPalindrome) {
            System.out.println("The array is a palindrome.");
        } else {
            System.out.println("The array is not a palindrome.");
        }
    }

    public static void main(String[] args) {
        int[] numbers1 = {1, 2, 3, 2, 1};
        int[] numbers2 = {1, 2, 3, 4, 5};
        int[] numbers3 = {1, 2, 0, 4, 5};
        checkArrayPalindrome(numbers1);
        checkArrayPalindrome(numbers2);
        checkArrayPalindrome(numbers3);
    }
}
