package hw4;

public class PalindromeChecker {
    public static void checkPalindrome(String str) {
        String cleanStr = str.replaceAll("\\s+", "").toLowerCase();
        int left = 0;
        int right = cleanStr.length() - 1;
        boolean isPalindrome = true;
        while (left < right) {
            if (cleanStr.charAt(left) != cleanStr.charAt(right)) {
                isPalindrome = false;
                break;
            }
            left++;
            right--;
        }
        if (isPalindrome) {
            System.out.println("String \"" + str + "\"  is a palindrome.");
        } else {
            System.out.println("String \"" + str + "\"  is not a palindrome.");
        }
    }

    public static void main(String[] args) {
        String inputString1 = "Pop";
        String inputString2 = "";
        String inputString3 = "As the crow flies";

        checkPalindrome(inputString1);
        checkPalindrome(inputString2);
        checkPalindrome(inputString3);
    }
}